import pygame
from pygame.locals import *
import time

pygame.init()

def game_opening(screen):
    font = pygame.font.Font(None, 50)
    text = font.render('Tic Tac Toe', 1, (255, 255, 255))
    screen.blit(text, (75, 150))
    pygame.display.flip()
    time.sleep(1)

def initialize_board(screen):
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((209, 203, 203))

    # vertical lines...
    pygame.draw.line(background, (0, 0, 0), (100, 0), (100, 300), 2)
    pygame.draw.line(background, (0, 0, 0), (200, 0), (200, 300), 2)

    # horizontal lines...
    pygame.draw.line(background, (0, 0, 0), (0, 100), (300, 100), 2)
    pygame.draw.line(background, (0, 0, 0), (0, 200), (300, 200), 2)

    return background


def game_status(board, turn, winner, grid):

    # global turn, winner,grid

    if ((None not in grid[0] and
         None not in grid[1] and
         None not in grid[2])and
            winner == None):
        message = "Match Tied!!"
    elif winner == None:
        message = turn + "'s turn"
    else:
        message = winner + ' Won!!!'

    font = pygame.font.Font(None, 20)
    text = font.render(message, 1, (0, 0, 0))

    board.fill((255, 255, 255), (0, 300, 300, 25))
    board.blit(text, (120, 301))


def update_display(screen, board, turn, winner, grid):
    game_status(board, turn, winner, grid)
    screen.blit(board, (0, 0))
    pygame.display.flip()


def board_tile_position(clickX, clickY):
    if (clickX < 100):
        col = 0
    elif (clickX < 200):
        col = 1
    else:
        col = 2

    if (clickY < 100):
        row = 0
    elif (clickY < 200):
        row = 1
    else:
        row = 2

    return (row, col)


def draw(board, boardRow, boardCol, player,grid):
    # global grid, turn

    centerX = ((boardCol) * 100) + 50
    centerY = ((boardRow) * 100) + 50

    if (player == 'X'):
        pygame.draw.line(board, (0, 0, 0), (centerX - 25, centerY - 25),
                         (centerX + 25, centerY + 25), 2)
        pygame.draw.line(board, (0, 0, 0), (centerX + 25, centerY - 25),
                         (centerX - 25, centerY + 25), 2)
    else:
        pygame.draw.circle(board, (0, 0, 0), (centerX, centerY), 38, 2)

    grid[boardRow][boardCol] = player
    return grid


def player_toggle(board,turn, grid):
    # global turn, grid

    (clickX, clickY) = pygame.mouse.get_pos()
    row, col = board_tile_position(clickX, clickY)

    if grid[row][col] == None:
        grid = draw(board, row, col,turn, grid)
    else:
        return

    if turn == 'X':
        turn = 'O'
    else:
        turn = 'X'
    
    return turn, grid


def check_winner(board, grid, winner):
    # global grid,winner
    # Row Check
    for row in range(0, 3):
        if ((grid[row][0] == grid[row][1] == grid[row][2]) and
                (grid[row][0] is not None)):
            winner = grid[row][0]
            pygame.draw.line(board, (0, 0, 0), (0, (row + 1)*100 - 50),
                             (300, (row + 1)*100 - 50), 2)
            break
    # Column Check
    for col in range(0, 3):
        if ((grid[0][col] == grid[1][col] == grid[2][col]) and
                (grid[0][col] is not None)):
            winner = grid[0][col]
            pygame.draw.line(board, (0, 0, 0), ((col + 1) * 100 - 50, 0),
                             ((col + 1) * 100 - 50, 300), 2)
            break
    # Diagonal Check
    # Left to Right
    if ((grid[0][0] == grid[1][1] == grid[2][2]) and
            (grid[0][0] is not None)):
        winner = grid[0][0]
        pygame.draw.line(board, (0, 0, 0), (50, 50), (250, 250), 2)

    if ((grid[0][2] == grid[1][1] == grid[2][0]) and
            (grid[0][2] is not None)):
        winner = grid[0][2]
        pygame.draw.line(board, (0, 0, 0), (250, 50), (50, 250), 2)

    return winner


def game_loop():

    screen = pygame.display.set_mode((300, 325))
    pygame.display.set_caption('Tic-Tac-Toe')

    turn = 'X'
    grid = [[None, None, None],
            [None, None, None],
            [None, None, None]]

    winner = None

    game_opening(screen)

    board = initialize_board(screen)
    running = True

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if winner == None:
                    turn, grid = player_toggle(board, turn, grid)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    game_loop()

        winner = check_winner(board,grid, winner)
        update_display(screen, board, turn, winner, grid)


game_loop()
