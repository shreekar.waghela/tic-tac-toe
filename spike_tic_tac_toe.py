import pygame
from pygame.locals import *

pygame.init()

screen = pygame.display.set_mode((300, 325))
pygame.display.set_caption('Tic-Tac-Toe')

turn = 'X'
grid = [[None, None, None],
        [None, None, None],
        [None, None, None]]

winner = None

#Initialize board
background = pygame.Surface(screen.get_size())
background = background.convert()
background.fill((209, 203, 203))
# vertical lines...
pygame.draw.line(background, (0, 0, 0), (100, 0), (100, 300), 2)
pygame.draw.line(background, (0, 0, 0), (200, 0), (200, 300), 2)
# horizontal lines...
pygame.draw.line(background, (0, 0, 0), (0, 100), (300, 100), 2)
pygame.draw.line(background, (0, 0, 0), (0, 200), (300, 200), 2)

#Game_status
def game_status(board):
    global turn, winner, background

    if ((None not in grid[0] and
         None not in grid[1] and
         None not in grid[2])and
            winner == None):
        message = "Match Tied!!"
    elif winner == None:
        message = turn + "'s turn"
    else:
        message = winner + ' Won!!!'

    font = pygame.font.Font(None, 20)
    text = font.render(message, 1, (10, 10, 10))

    board.fill((255, 255, 255), (0, 300, 300, 24))
    board.blit(text, (120, 301))

def player_toggle(board):
    global turn, grid

    (clickX, clickY) = pygame.mouse.get_pos()
    if (clickX < 100):
        col = 0
    elif (clickX < 200):
        col = 1
    else:
        col = 2

    if (clickY < 100):
        row = 0
    elif (clickY < 200):
        row = 1
    else:
        row = 2

    if grid[row][col] == None:
        draw(board, row, col, turn)
    else:
        return

    if turn == 'X':
        turn = 'O'
    else:
        turn = 'X'

def draw(board, boardRow, boardCol, player):
    centerX = ((boardCol) * 100) + 50
    centerY = ((boardRow) * 100) + 50

    if (player == 'X'):
        pygame.draw.line(board, (0, 0, 0), (centerX - 25, centerY - 25),
                         (centerX + 25, centerY + 25), 2)
        pygame.draw.line(board, (0, 0, 0), (centerX + 25, centerY - 25),
                         (centerX - 25, centerY + 25), 2)
    else:
        pygame.draw.circle(board, (0, 0, 0), (centerX, centerY), 38, 2)

    grid[boardRow][boardCol] = player

def game_loop():

    global background, winner
    running = True

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                player_toggle(background)

    for row in range(0, 3):
        if ((grid[row][0] == grid[row][1] == grid[row][2]) and
                (grid[row][0] is not None)):
            winner = grid[row][0]
            pygame.draw.line(background, (0, 0, 0), (0, (row + 1)*100 - 50),
                             (300, (row + 1)*100 - 50), 2)
            break
    # Column Check
    for col in range(0, 3):
        if ((grid[0][col] == grid[1][col] == grid[2][col]) and
                (grid[0][col] is not None)):
            winner = grid[0][col]
            pygame.draw.line(background, (0, 0, 0), ((col + 1) * 100 - 50, 0),
                             ((col + 1) * 100 - 50, 300), 2)
            break
    # Diagonal Check
    # Left to Right
    if ((grid[0][0] == grid[1][1] == grid[2][2]) and
            (grid[0][0] is not None)):
        winner = grid[0][0]
        pygame.draw.line(background, (0, 0, 0), (50, 50), (250, 250), 2)

    if ((grid[0][2] == grid[1][1] == grid[2][0]) and
            (grid[0][2] is not None)):
        winner = grid[0][2]
        pygame.draw.line(background, (0, 0, 0), (250, 50), (50, 250), 2)

    game_status(background)
    screen.blit(background, (0, 0))
    pygame.display.flip()

game_loop()