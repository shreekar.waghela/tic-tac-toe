## Tic Tac Toe

This the classic Tic-Tac-Toe game.
Match either 'X' or 'O' in the row, column or diagonal to win.
If there is no pattern the match is draw. 

### Game start
![Game Start](./tic1.png)
### X turn
![X turn](./tic2.png)
### O turn
![O turn](./tic3.png)
### X win
![X win](./tic4.png)
### O win
![O win](./tic5.png)
### Match tied
![Match tied](./tic6.png)
